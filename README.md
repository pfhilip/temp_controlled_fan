A temperature controlled fan for the ATMega324PA.
 


It features an simple lcd dot matrix display which shows the current mode (manual or automatic), the current temperature and the speed of the fan. 



The fan speed can be controlled either by the temperature sensor or manually via the potentiometer. 



Pressing the button toggles between the two modes. 



The screen is wrote to via I2C/TWI. Search Amazon for an I2C LCD adapter...




PIN40  = Potentiometer

PIN39 = tmp36

PIN03 = push button

PIN04 = motor

