/*
 * inputs.c
 *
 * Created: 22/08/2016 01:48:20
 *  Author: Philip
 */
#include "temp_controlled_fan.h"

uint8_t get_mode(void){
	return 0;
}

void set_mode(uint8_t mode){
	_delay_ms(20);
	if(mode){
		print_to_screen("AUTO  ", MOD_POS);
	} else {
		print_to_screen("MANUAL  ", MOD_POS);
	}
}

void set_speed(uint8_t speed){
	_delay_ms(20);
	switch(speed){
		case 0:
			print_to_screen("1", SPEED_POS);
			break;
		case 1:
			print_to_screen("2", SPEED_POS);
			break;
		case 2:
			print_to_screen("3", SPEED_POS);
			break;
		case 3:
			print_to_screen("4", SPEED_POS);
			break;
		case 4:
			print_to_screen("5", SPEED_POS);
			break;
		default:
			print_to_screen("0", SPEED_POS);
			break;		
	}
}

void set_temp(int8_t temp){
	char str[9] = {0};
	sprintf(str, "%hd", temp);
	str[8] = '\0';
	print_to_screen("     ", TEMP_POS);
	print_to_screen(str, TEMP_POS);
}

int8_t read_temperature(void){
	_delay_ms(20);
	int8_t temp = 0;
	uint16_t reading = 0;
	reading = ADCL;
	reading |= (ADCH<<8);
	temp = (((reading * 4.8875855f) - 500.0f)/10.0f);
	return temp;
} 

uint8_t read_pot(void){
	
	uint8_t speed = 0;
	
	if(global_fan_mode == MANUAL){
		ADCSRA &= ~(1<<ADEN | 1<<ADSC | 1<<ADATE);
		ADMUX &= ~(1<<0);
		ADCSRA |= 1<<ADEN | 1<<ADSC | 1<<ADATE;
		_delay_ms(20);
		speed = manual_speed();
		
		
	} else {
		ADCSRA &= ~(1<<ADEN | 1<<ADSC | 1<<ADATE);
		ADMUX |= (1<<0);
		ADCSRA |= 1<<ADEN | 1<<ADSC | 1<<ADATE;
		_delay_ms(20);
		
		speed = auto_speed();
	}	
	
	return speed;
}

uint8_t manual_speed(void){
	
	uint8_t speed;
	uint16_t reading = ADCL;
	reading |= (ADCH<<8);
	
	if(reading > 800){
		speed = 4;
		OCR0A = 0x05;
	} else if(reading < 800 && reading > 600){
		speed = 3;
		OCR0A = 0x45;
	} else if(reading < 600 && reading > 400){
		speed = 2;
		OCR0A = 0x80;
	} else if(reading < 400 && reading > 200){
		speed = 1;
		OCR0A = 0xB0;
	} else if(reading < 200 && reading > 0){
		speed = 0;
		OCR0A = 0xF0;
	}
	return speed;
}

uint8_t auto_speed(void){
	
	uint8_t speed;
	uint16_t reading = ADCL;
	reading |= (ADCH<<8);
	
	if(reading > 173){
		speed = 4;
		OCR0A = 0x05;
	} else if(reading < 173 && reading >= 165){
		speed = 3;
		OCR0A = 0x40;
	} else if(reading < 165 && reading >= 161){
		speed = 2;
		OCR0A = 0x80;
	} else if(reading < 161 && reading >= 157){
		speed = 1;
		OCR0A = 0xB0;
	} else if(reading < 157 && reading >= 0){
		speed = 0;
		OCR0A = 0xF0;
	}
	return speed;
}
