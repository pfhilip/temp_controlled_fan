/*
 * lcd_control.c
 *
 * Created: 18/08/2016 21:40:25
 *  Author: Philip
 */

/*  Default A0=A1=A2=1 giving I2C address = 0x27
 *	P0 = LCD RS
 *	P1 = LCD R/W
 *	P2 = LCD E
 *	P3 = LCD backlight (+ve logic) (via physical jumper enable/disable)
 *	P4 = LCD D4
 *	P5 = LCD D5
 *	P6 = LCD D6
 *	P7 = LCD D7
*/
#include "temp_controlled_fan.h"

static void enable(void);
static inline void wait_for_int_flag(void);

static void enable(void){
	TWDR |= 1<<EN;
	TWCR |= 1<<TWINT | 1<<TWEN;
	wait_for_int_flag();
	//_delay_ms(10);
	TWDR &= ~(1<<EN);
	TWCR |= 1<<TWINT | 1<<TWEN;
	wait_for_int_flag();
}

void send_command(uint8_t command){
	TWDR = command;
	TWDR |= 1<<BL;
	TWDR &= ~(1<<RS | 1<<RW);
	enable();
	TWDR &= ~(0xF0);
	TWDR |= command<<4;
	enable();
}

static inline void wait_for_int_flag(void){
	while(!(TWCR & (1<<TWINT)));
}

void send_character(uint8_t character){
	TWDR = character;
	TWDR &= ~(0x0F);
	TWDR |= 1<<RS | 1<<BL;
	enable();
	TWDR &= ~(0xF0);
	TWDR |= character<<4;
	enable();
}

void begin_transmission(void){
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	TWCR &= ~(1<<TWSTO);
}

void send_address(uint8_t address){
	wait_for_int_flag();
	TWDR = address<<1;
	TWDR &= ~(1<<0);
	TWCR |= 1<<TWINT | 1<<TWEN;
	TWCR &= ~(1<<TWSTA);
}

void end_transmission(void){
	TWCR = (1<<TWINT)|(1<<TWSTO);
}

void four_bit_mode(void){
	send_command(0x30);
	send_command(0x30);
	send_command(0x20);
}

void move_cursor_to(uint8_t address){
	send_command(address);
}

void print_to_screen(char* message, uint8_t location){
	move_cursor_to(location);
	for(int i = 0; message[i] != '\0' ;++i){
		send_character(message[i]);
	}
}

void print_init(void){	
	char* mode = "Mode:MANUAL    ";
	char* temp = "Temp";
	char* speed = "Speed: 1";
	
	int i;
	
	move_cursor_to(MODE);
	for(i = 0; mode[i] != '\0';++i){
		send_character(mode[i]);
	}
	
	move_cursor_to(TEMP);
	for(i = 0; temp[i] != '\0';++i){
		send_character(temp[i]);
	}
	
	move_cursor_to(SPEED);
	for(i = 0; speed[i] != '\0';++i){
		send_character(speed[i]);
	}
}
