/*
 * temp_controlled_fan.c
 *
 * Created: 18/08/2016 20:56:49
 * Author : Philip
 */ 

#include "temp_controlled_fan.h"

volatile uint8_t global_fan_mode = MANUAL;
volatile uint8_t temp_flag = UNSET;

static void analog_init(void);
static void button_poll(void);
static void screen_init(void);
static void counter_init(void);
	
int main(void){
	
	DDRB &= ~(1<<1);
	PORTB |= 1<<1;
	
	DDRB |= 1<<3;
	PORTB |= 1<<3;
	
	counter_init();
	analog_init();
	screen_init();
	sei();
	
	while(1){
		button_poll();
		set_speed(read_pot());
		
		if(temp_flag){
				
			ADCSRA &= ~(1<<ADEN | 1<<ADSC | 1<<ADATE);
			ADMUX |= (1<<0);
			ADCSRA |= 1<<ADEN | 1<<ADSC | 1<<ADATE;
				
			set_temp(read_temperature());
			temp_flag = UNSET;
		}
	}	
	
	end_transmission();
	
	return 0;
}

static void screen_init(void){
		begin_transmission();
		send_address(ADDRESS);
		send_command(0x0F);
		send_command(HOME);
		send_character(0x65);
		print_init();
}

static void analog_init(void){
	ADMUX |= 1<<REFS0;
	ADMUX &= ~(1<<REFS1);
	
	ADCSRA |= 1<<ADEN | 1<<ADSC | 1<<ADATE;
}

static void counter_init(void){
	//COUNTER for temperature read interrupt
	TCNT1 = 0x00;
	OCR1A |= 0xF0FF;//TOP
	
	TCCR1A &= ~(1<<WGM10 | 1<<WGM11);
		
	TCCR1B |= (1<<WGM12 | 1<<CS10 | 1<<CS12 );
	TCCR1B &= ~(1<<CS11 | 1<<WGM13 );
	
	TIMSK1 |= 1<<OCIE1A;
	//COUNTER for motor speed control
	TCNT0 = 0x00;
	OCR0A = 0x05;
	
	TCCR0A |= ( 1<<COM0A0 | 1<<COM0A1 | 1<<WGM00);
	TCCR0B |= (1<<CS00);		
}

static void button_poll(void){
	
	static uint16_t i;//for debouncing
	
	if(!(PINB & 1<<1)){
		++i;
		if(i > 3){
			if(global_fan_mode == MANUAL){
				set_mode(AUTO);
				global_fan_mode = AUTO;
				i = 0;
				//Select the tmp36
				ADCSRA &= ~(1<<ADEN | 1<<ADSC | 1<<ADATE);
				ADMUX |= (1<<0);
				ADCSRA |= 1<<ADEN | 1<<ADSC | 1<<ADATE;
				
				while(i < 30){
					if(PINB & 1<<1)
					++i;
				}
								
			} else if(global_fan_mode == AUTO) {
				set_mode(MANUAL);
				global_fan_mode = MANUAL;
				i = 0;
				//select the pot
				ADCSRA &= ~(1<<ADEN | 1<<ADSC | 1<<ADATE);
				ADMUX &= ~(1<<0);
				ADCSRA |= 1<<ADEN | 1<<ADSC | 1<<ADATE;
				
				while(i < 30){
					if(PINB & 1<<1)
					++i;
				}
			}
		}
	}
}

ISR(TIMER1_COMPA_vect){
	temp_flag = SET;
}

