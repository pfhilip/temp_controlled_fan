/*
 * temp_controlled_fan.h
 *
 * Created: 19/08/2016 00:18:49
 *  Author: Philip
 */ 

#ifndef TEMP_CONTROLLED_FAN_H_
#define TEMP_CONTROLLED_FAN_H_

#define F_CPU 8000000
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>
#include <avr/interrupt.h>

#define RS 0
#define RW 1
#define EN 2
#define BL 3

#define ADDRESS 0x27

#define CLEAR 0x01
#define HOME  0x02

#define MODE		0x80
#define SPEED		0xC0
#define TEMP		0xC9
#define MOD_POS		0x85
#define SPEED_POS	0xC7
#define TEMP_POS	0xCD

#define MANUAL	0
#define AUTO	1

#define SET		1
#define UNSET	0

extern volatile uint8_t global_fan_mode;
extern volatile uint8_t temp_flag;

void send_command(uint8_t command);
void send_character(uint8_t character);
void four_bit_mode(void);
void begin_transmission(void);
void end_transmission(void);
void send_address(uint8_t address);
void move_cursor_to(uint8_t address);
void print_to_screen(char* message, uint8_t location);
void print_init(void);

void set_mode(uint8_t mode);
void set_speed(uint8_t speed);
void set_temp(int8_t temp);
int8_t read_temperature(void);
uint8_t read_pot(void);
uint8_t manual_speed(void);
uint8_t auto_speed(void);

#endif /* TEMP_CONTROLLED_FAN_H_ */